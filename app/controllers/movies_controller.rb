class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index
    @all_ratings = Movie.Ratings
    new_ratings = params[:ratings]||session[:ratings]||{'G'=>true,'PG'=>true,'PG-13'=>true,'R'=>true}
    new_sort_id = params[:sort_id] || session[:sort_id]
    new_sort_type = params[:sort_type] || session[:sort_type]

    if session[:keep_flash] == true
      flash.keep :notice
      session[:keep_flash] = false
    end
    # If new params have been set, redirect to preserve RESTful URLs
    if (params[:ratings] == nil && new_ratings != nil) ||
       (params[:sort_id] == nil && new_sort_id != nil)
      redirect_to movies_path ratings:new_ratings, sort_id:new_sort_id, sort_type:new_sort_type
      return false
    end

    @checked_ratings, @sort_id, @sort_type = new_ratings, new_sort_id, new_sort_type
    session[:ratings] = new_ratings
    @find_ratings = @checked_ratings.keys
    if @sort_id != nil && (@sort_id == "title" || @sort_id == "release_date")
      session[:sort_id] = @sort_id      # remember it
      session[:sort_type] = @sort_type
      sort_str = %Q{#@sort_id #@sort_type}
      @movies = Movie.all order: sort_str, :conditions => {:rating => @find_ratings}
    else
      @movies = Movie.all :conditions => {:rating => @find_ratings}
    end
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    session[:keep_flash] = true
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    session[:keep_flash] = true
    redirect_to movies_path
  end

end
